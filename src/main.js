import Vue from 'vue'
import App from './App'
import config from '@/config.js'
import store from '@/store'

// #ifdef MP-UNIONPAY
// 集成云闪付授权组件 官方原来的有bug，手动修改vue-cup-ui.common.js handleClick 方法
import VueCupUi from 'vue-cup-ui';
import 'vue-cup-ui/lib/vue-cup-ui.css';
Vue.use(VueCupUi);
// #endif

// 云闪付小程序设置标题
Vue.prototype.$setNavTitle = (title, backBtnVisible = 1) => {
	// #ifdef MP-UNIONPAY
	upsdk.pluginReady(() => {
		upsdk.setTitleStyle({
			// 可选，black-黑色主题，white-白色主题
			appletStyle: 'black',
			// 可选，左侧返回按钮是否显示。'0'不显示，'1'显示，不传或空则默认显示
			backBtnVisible: backBtnVisible ? '1' : '0'
		});

		upsdk.setNavigationBarTitle({
			title
		});
	});
	// #endif
}

Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$ossDomain = config.OSS_DOMAIN;

let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);

// 引入uView
import uView from 'uview-ui'
Vue.use(uView);

// #ifdef H5
import wechat from '@/utils/wxH5/jssdk.js';
if (wechat.isWechat()) {
	Vue.prototype.$wechat = wechat;
}
// #endif

// #ifndef H5
// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare);
// #endif

const app = new Vue({
	store,
	...App
})

// http拦截器
import httpInterceptor from '@/utils/http.interceptor.js'
Vue.use(httpInterceptor, app)

// http接口API集中管理
import httpApi from '@/utils/http.api.js'
Vue.use(httpApi, app)

app.$mount()
