/**
 * http接口API集中管理
 */
const install = (Vue, vm) => {
	// 微信小程序login接口
	let wxLogin = (code) => vm.$u.get('/wxLogin/' + code);
	// 微信小程序手机号授权登录
	let wxGetPhoneNumber = (params = {}) => vm.$u.post('/wxGetPhoneNumber', params);
	// 云闪付手机号授权登录
	let appletAuth = (code, domain) => vm.$u.get('/admin/user/ysfauth', { code, domain });
	// 获取token
	let appletAuthTest = () => vm.$u.get('/admin/user/ysfauth2');

	vm.$u.api = {
		wxLogin,
		wxGetPhoneNumber,
		appletAuth,
		appletAuthTest,
	}
}

export default {
	install
}
