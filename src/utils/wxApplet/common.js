import Vue from 'vue';
import config from '@/config.js'

// 获取token
const appletAuthTest = () => {
	return new Promise((resolve, reject) => {
		Vue.prototype.$u.api.appletAuthTest().then(res => {
			if (res.code == 0) {
				Vue.prototype.$u.vuex('vuex_token', res.data.access_token);
				resolve()
			}
		});
	})
}

const login = () => {
	return new Promise((resolve, reject) => {
		// 本地开发环境
		if (process.env.NODE_ENV == "development") {
			appletAuthTest().then(() => {
				let openid = 'DrIB8Xkf3y/VM0043U7xi3rerKo1dNMGhyk4j1/i4TJzBg/TT2R3Y46Sn/0mQj1q/';
				let phone = '15255556666';
				Vue.prototype.$u.vuex('vuex_openid', openid);
				Vue.prototype.$u.vuex('vuex_phone', phone);
			})
		} else {
			// #ifdef MP-UNIONPAY
			upsdk.pluginReady(() => {
				// 云闪付静默授权
				upsdk.appletAuth({
					success: (res) => {
						// 云闪付手机号授权登录
						Vue.prototype.$u.api.appletAuth(res.code, config.DOMAIN).then(respones => {
							if (respones.code != 0) return Vue.prototype.$u.toast('静默授权失败');

							let data = respones.data;
							let openid = '',
								mobile = '';
							if (data.openid.length && data.openid[0]) {
								openid = data.openid[0];
								Vue.prototype.$u.vuex('vuex_openid', openid);
							}
							if (data.mobile.length && data.mobile[0]) {
								mobile = data.openid[0];
								Vue.prototype.$u.vuex('vuex_phone', mobile);
							}
						})
					},
					fail: (err) => {
						console.log('静默授权错误：', err)
						Vue.prototype.$u.toast('静默授权失败：' + err.errmsg);
					}
				})
			})
			// #endif
		}
	})
}

/**
 * 获取登录凭证
 * @return {boolean} true:已过期、false:未过期
 */
const wxLogin = () => {
	return new Promise((resolve, reject) => {
		// #ifdef MP-WEIXIN
		uni.checkSession({
			success: res => {
				//session_key 未过期，并且在本生命周期一直有效
				let openid = Vue.prototype.$store.state.vuex_openid;
				if (!openid) {
					login().then(() => { resolve() })
				} else {
					resolve(false)
				}
			},
			fail: err => {
				// session_key 已经失效，需要重新执行登录流程
				login().then(() => { resolve() })
			}
		})
		// #endif

		// #ifdef MP-UNIONPAY
		login().then(() => { resolve() })
		// #endif
	})
}

/**
 * 退出登录
 */
const logout = () => {
	return new Promise((resolve, reject) => {
		Vue.prototype.$u.vuex('vuex_token', '');
		Vue.prototype.$u.vuex('vuex_userInfo', '');
		Vue.prototype.$u.vuex('vuex_phone', '');
		resolve(true);
	})
}

/**
 * 微信手机号快捷登录
 * @param {object} e 回调函数的整个参数
 * @return {object} { true: '允许授权｜已授权过了 可以继续往下执行其他业务逻辑', false: '拒绝授权' }
 */
const getPhoneNumber = (e) => {
	return new Promise((resolve, reject) => {
		let detail = e.detail;
		if (detail.errMsg == "getPhoneNumber:ok") {
			let openid = Vue.prototype.$store.state.vuex_openid;
			if (!openid) {
				Vue.prototype.$u.toast('openid为空！请先调用login接口');
				resolve(false);
			}

			Vue.prototype.$u.api.wxGetPhoneNumber({
				code: detail.code,
				openid
			}).then(data => {
				Vue.prototype.$u.vuex('vuex_phone', data.phones);
				Vue.prototype.$u.vuex('vuex_token', data.token);
				resolve(true);
			})
		} else {
			// 拒绝授权
			if (detail.errMsg == 'getPhoneNumber:fail user deny' || detail.errMsg == 'getPhoneNumber:fail:user deny') {
				Vue.prototype.$u.toast('授权登录后才能体验完整的功能哦', 2500);
			} else if (detail.errMsg == 'getPhoneNumber:fail no permission') {
				Vue.prototype.$u.toast('该小程序暂无权限获取用户的手机号！', 2500);
			}
			resolve(false);
		}
	})
}

export default {
	appletAuthTest,
	wxLogin,
	logout,
	getPhoneNumber
}
